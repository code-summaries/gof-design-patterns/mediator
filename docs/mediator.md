<div align="center">
  <h1>Mediator</h1>
</div>

<div align="center">
  <img src="mediator_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Mediator is a behavioral pattern that forces objects to only collaborate via the mediator.**

### Real-World Analogy

_Air traffic controller._

Pilots of aircraft (colleagues) that approach the airport don’t communicate directly with each other, but with an air
traffic controller (mediator). Without it, pilots would need to be aware of every plane in the vicinity.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

• Mediator (DialogDirector)- defines an interface for communicating with Colleague objects. • ConcreteMediator (
FontDialogDirector)- implements cooperative behavior by coordinating Colleague objects.- knows and maintains its
colleagues. • Colleague classes (ListBox, EntryField)- each Colleague class knows its Mediator object.- each colleague
communicates with its mediator whenever it would have otherwise communicated with another colleague

### Collaborations

...
Colleagues send and receive requests from a Mediator object. The mediator
implements the cooperative behavior by routing requests between the appropriate colleague(s).

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Use the Mediator pattern when
• a set of objects communicatein well-defined but complexways.The resulting
interdependencies are unstructured and difficult to understand.
• reusing an object is difficult because itrefers to and communicateswithmany
other objects.
• a behavior that's distributed between several classes should be customizable
without a lot of subclassing.

### Motivation

- ...

Object-oriented design encourages the distribution of behavior among objects.
Such distribution can result in an object structure with many connections between
objects; in the worst case, every object ends up knowing about every other.

Though partitioning a system into many objects generally enhances reusability,
proliferating interconnections tend to reduce it again. Lots of interconnections
make it less likely that an object can work without the support of others—the
system acts as though it were monolithic. Moreover, it can be difficult to change
the system'sbehavior in any significant way,since behavior is distributed among
many objects. As a result, you may be forced to define many subclasses to customize the system's behavior.

As an example, consider the implementation of dialog boxes in a graphical user
interface. A dialog box uses a window to present a collection of widgets such as
buttons, menus, and entry fields, as shown here

Often there are dependencies between the widgets in the dialog. For example,
a button gets disabled when a certain entry field is empty. Selecting an entry
in a list of choices called a list box might change the contents of an entry field.
Conversely typing text into the entry field might automatically select one or more
corresponding entries in the list box. Once text appears in the entry field, other
buttons may become enabled that let the user do something with the text,such as
changing or deleting the thing to which it refers.

Different dialog boxeswill have different dependencies between widgets. So even
though dialogs display the same kinds of widgets, they can't simply reuse stock
widget classes; they have to be customized to reflect dialog-specific dependencies.
Customizing them individually by subclassing will be tedious, since many classes
are involved.

You can avoid these problems by encapsulating collective behavior in a separate
mediator object. A mediator is responsible for controlling and coordinating the
interactions of a group of objects. The mediator serves as an intermediary that
keeps objectsin the group from referring to each other explicitly. The objects only
know the mediator, thereby reducing the number of interconnections.

For example, FontDialogDirector can be the mediator between the widgets in
a dialog box. A FontDialogDirector object knows the widgets in a dialog and
coordinates their interaction. It acts as a hub of communication for widgets:

The following interaction diagram illustrates how the objects cooperate to handle
a change in a list box's selection

Here's the succession of events by which a list box's selection passes to an entry
field:

1. The list box tells its director that it's changed.
2. The director gets the selection from the list box.
3. The director passesthe selection to the entry field.
4. Now that the entry field contains some text, the director enables button(s)
   for initiating an action (e.g., "demibold," "oblique").

Note how the director mediates between the list box and the entry field. Widgets
communicate with each other only indirectly, through the director. They don't
have toknow about each other; allthey know isthe director.Furthermore,because
the behavior islocalized in one class, it can be changed or replaced by extending
or replacing that class.

Here's how the FontDialogDirector abstraction can be integrated into a class
library

DialogDirector is an abstract class that defines the overall behavior of a dialog. Clients call the ShowDialog
operation to display the dialog on the screen.
CreateWidgets is an abstract operation for creating the widgets of a dialog. WidgetChanged is another abstract
operation; widgets call it to inform their director
that they have changed. DialogDirector subclasses override CreateWidgets to create the proper widgets, and they
override WidgetChanged to handle the changes.

---

Use the Mediator pattern when it’s hard to change some of the classes because they are tightly coupled to a bunch of
other classes.

The pattern lets you extract all the relationships between classes into a separate class, isolating any changes to a
specific component from the rest of the components.

Use the pattern when you can’t reuse a component in a different program because it’s too dependent on other components.

After you apply the Mediator, individual components become unaware of the other components. They could still communicate
with each other, albeit indirectly, through a mediator object. To reuse a component in a different app, you need to
provide it with a new mediator class.

Use the Mediator when you find yourself creating tons of component subclasses just to reuse some basic behavior in
various contexts.

Since all relations between components are contained within the mediator, it’s easy to define entirely new ways for
these components to collaborate by introducing new mediator classes, without having to change the components themselves.

### Known Uses

- ...

GUI Systems: In user interfaces where multiple components interact, such as a graphical application, the mediator
manages the communication between these components. For instance, in a window with multiple widgets (buttons, text
fields, etc.), the mediator can handle interactions between these elements.

Air Traffic Control Systems: In a system controlling air traffic, the mediator could manage communication between
various aircraft, air traffic controllers, and other elements involved in ensuring safe and efficient air travel.

Chat Applications: In a chat system, the mediator manages the communication between different users. Instead of users
directly communicating with each other, their messages are sent through the mediator, which distributes them to the
intended recipients.

Event-Driven Systems: In systems where events trigger actions across multiple components, the mediator pattern can help
manage these events and their effects, ensuring proper handling and communication among various parts of the system.

Stock Exchange Systems: A mediator can manage the communication and transactions between various stock exchanges,
brokers, and traders, ensuring that trades are executed properly and relevant information is shared among the concerned
parties.

Smart Home Systems: In a smart home setup with various IoT devices like sensors, thermostats, lights, etc., a mediator
can coordinate interactions among these devices based on user preferences or certain conditions (e.g., turning off
lights when no motion is detected).

Multiplayer Games: In multiplayer games, the mediator pattern can manage interactions between different players,
handling actions, updates, and ensuring synchronization between different clients.

Hospital Information Systems: In a complex healthcare system, a mediator could manage communication between different
departments, such as connecting patient information between doctors, nurses, laboratories, and administrative staff.

Smart Traffic Management Systems: In a city's traffic management system, a mediator can coordinate traffic signals,
sensors, and other devices to optimize traffic flow, considering real-time data and adjusting signals based on current
conditions.

Financial Trading Systems: Within financial trading platforms, a mediator can handle communication between various
modules such as order matching, risk management, and trade execution systems, ensuring smooth and secure transactions.

Enterprise Resource Planning (ERP) Systems: In large organizations, ERP systems often consist of multiple modules (
finance, HR, inventory, etc.). A mediator can facilitate communication and data exchange between these modules, ensuring
consistent and updated information across the organization.

Supply Chain Management: Coordinating different elements in a supply chain, such as suppliers, manufacturers,
distributors, and retailers, can be effectively managed using a mediator to optimize processes, inventory levels, and
deliveries.

Multi-Platform Integration: In software systems that need to integrate with various external services or APIs across
different platforms (web, mobile, desktop), a mediator can handle interactions, data exchange, and compatibility issues
between these platforms.

Smart Energy Grids: Mediators can play a role in managing communication between different components of a smart energy
grid, such as power generation sources, storage systems, distribution networks, and consumers, to ensure efficient and
reliable energy delivery.

Aircraft Control Systems: In complex aircraft systems, a mediator can manage communication between various onboard
systems, sensors, controls, and the flight crew, ensuring that all systems work together seamlessly for safe and
efficient flight operations.

java.util.Timer (all scheduleXXX() methods)
java.util.concurrent.Executor#execute()
java.util.concurrent.ExecutorService (the invokeXXX() and submit() methods)
java.util.concurrent.ScheduledExecutorService (all scheduleXXX() methods)
java.lang.reflect.Method#invoke()

Both ET++ [WGM88] and the THINK C class library [Sym93b] use director-like
objects in dialogs as mediators between widgets.
The application architecture of Smalltalk/V for Windows is based on a media-
tor structure [LaL94]. In that environment, an application consists of a Window
containing a set of panes. The library contains several predefined Pane objects;
examples include TextPane, ListBox, Button, and so on. These panes can be used
without subclassing. An application developer only subclasses from ViewMan-
ager, a class that's responsible for doing inter-pane coordination. ViewManager is
the Mediator, and each pane only knows its view manager, which is considered
the "owner" of the pane. Panes don't refer to each other directly.
The following object diagram shows a snapshot of an application at run-time:
Smalltalk/V uses an event mechanism for Pane-ViewManager communication. A
pane generates an event when it wants to get information from the mediator or
when it wants to inform the mediator that something significant happened. An
event defines a symbol (e.g., #select) that identifies the event. To handle the
event, the view manager registers a method selector with the pane. This selector
is the event's handler; it will be invoked whenever the event occurs.
The following code excerpt shows how a ListPane object gets created inside a
ViewManager subclass and how ViewManager registers an event handler for the

```text
#select event:
self addSubpane: (ListPane new
paneName: 'myListPane';
owner: self;
when: #select perform: ttlistSelect:).
```

Another application of the Mediator pattern is in coordinating complex updates.
An example is the ChangeManager class mentioned in Observer (293). Change-
Manager mediates between subjects and observers to avoid redundant updates.
When an object changes, it notifies the ChangeManager, which in turn coordinates
the update by notifying the object's dependents.
A similar application appears in the Unidraw drawing framework [VL90] and uses
a class called CSolver to enforce connectivity constraints between "connectors."
Objects in graphical editors can appear to stick to one another in different ways.
Connectors are useful in applications that maintain connectivity automatically,
like diagram editors and circuit design systems. CSolver is a mediator between
connectors. It solves the connectivity constraints and updates the connectors'
positions to reflect them.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Association**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- How and which objects interact with each other.

### Solution to causes of redesign

- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The Mediator pattern has the followingbenefits and drawbacks:

1. It limits subclassing. A mediator localizes behavior that otherwise would be
   distributed among several objects. Changing this behavior requires subclassing Mediator only; Colleague classes can
   be reused as is.
2. It decouples colleagues. A mediator promotes loose coupling between colleagues. You can vary and reuse Colleague and
   Mediator classes independently.
3. It simplifies object protocols. A mediator replaces many-to-many interactions
   with one-to-many interactionsbetween themediator and its colleagues. Oneto-many relationships are easier to
   understand, maintain, and extend.
4. It abstracts how objects cooperate. Making mediation an independent concept
   and encapsulating it in an object lets you focus on how objectsinteract apart
   from their individual behavior. That can help clarify how objectsinteract in
   a system.
5. It centralizes control. The Mediator pattern trades complexity of interaction
   for complexity in the mediator. Because a mediator encapsulates protocols,
   it can become more complex than any individual colleague. This can make
   the mediator itself a monolith that's hard to maintain

Single Responsibility Principle. You can extract the communications between various components into a single place,
making it easier to comprehend and maintain.
Open/Closed Principle. You can introduce new mediators without having to change the actual components.
You can reduce coupling between various components of a program.
You can reuse individual components more easily.

Over time a mediator can evolve into a God Object.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Facade (185) differs from Mediator in that it abstracts a subsystem of objects
to provide a more convenient interface. Its protocol is unidirectional; that is,
Facade objects make requests of the subsystem classes but not vice versa. In
contrast, Mediator enables cooperative behavior that colleague objects don't or
can't provide, and the protocol is multidirectional.
Colleagues can communicate with the mediator using the Observer (293) pattern.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

(recognizeable by behavioral methods taking an instance of different abstract/interface type (usually using the command
pattern) which delegates/uses the given instance)

### Structure

```mermaid
classDiagram
    class Mediator {
        <<interface>>
        + colleagueChanged(colleague: Colleague): void
    }

    class ConcreteMediator {
        - colleagues: Colleague[]
        + addColleague(colleague: Colleague): void
        + colleagueChanged(colleague: Colleague): void
    }

    class Colleague {
        <<interface>>
        - mediator: Mediator
        + setMediator(mediator: Mediator): void
        + send(message: string): void
        + receive(message: string): void
    }

    class ConcreteColleague {
        + send(message: string): void
        + receive(message: string): void
    }

    Mediator <|.. ConcreteMediator: implements
    Mediator --o Colleague: aggregated by
    Colleague <|.. ConcreteColleague: implements
    Colleague --o ConcreteMediator: aggregated by

```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

The following implementation issues are relevant to the Mediator pattern:

1. Omitting the abstract Mediator class. There's no need to define an abstract
   Mediator class when colleagues work with only one mediator. The abstract
   coupling that the Mediator class provides lets colleagues work with different
   Mediator subclasses, and vice versa.
2. Colleague-Mediator communication. Colleagues have to communicate with
   their mediator when an event of interest occurs. One approach is to im-
   plement the Mediator as an Observer using the Observer (293) pattern. Col-
   league classes act as Subjects, sending notifications to the mediator whenever
   they change state. The mediator responds by propagating the effects of the
   change to other colleagues.
   Another approach defines a specialized notification interface in Mediator
   that lets colleagues be more direct in their communication. Smalltalk/V for
   Windows uses a form of delegation: When communicating with the media-
   tor, a colleague passes itself as an argument, allowing the mediator to identify
   the sender. The Sample Code uses this approach, and the Smalltalk/V im-
   plementation is discussed further in the Known Uses.

### Implementation

In the example we apply the mediator pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Mediator](https://refactoring.guru/design-patterns/mediator)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Geekific: The Mediator Pattern Explained and Implemented in Java ](https://youtu.be/35D5cBosD4c?si=5sO4sUYq8cOMytrT)
- [DotNet Core Central: Mediator Design Pattern (An Introduction for .NET Developers](https://youtu.be/UrFJUcIXTfc?si=No4DseOquJXkX-sG)

<br>
<br>
